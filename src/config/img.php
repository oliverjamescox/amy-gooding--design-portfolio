<?php
return [
    'defaultRatio' => '1/1',
    'transforms' => [
        'default' => [
            'format' => 'jpg',
            'sizes' => [
                ['width' => 700],
                ['width' => 500],
                ['width' => 450]
            ]
        ],
        'background' => [
            'format' => 'jpg',
            'sizes' => [
                ['width' => 1600, 'ratio' => 2 / 1 ],
                ['width' => 1000, 'ratio' => 2 / 1 ],
                ['width' => 700, 'ratio' => 1.5 / 1 ]
            ]
        ]
    ]
];