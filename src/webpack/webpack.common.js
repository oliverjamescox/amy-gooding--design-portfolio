const webpack = require('webpack');
const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

    entry: {
        app: path.resolve('./src/assets/js/app.js'),
    },

    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/assets/index.twig', 
            filename: "../../templates/_layouts/app.twig"
        }),
        new webpack.optimize.LimitChunkCountPlugin({
            maxChunks: 1
            })
    ]
  };
